#!/bin/bash

set -euo pipefail

api="${1:-https://coronavirus.cyberbits.eu/api}"
dashboards=$(curl -s ${api}/search | jq -r '.[].uid')

mkdir -p grafana
for dashboard in $dashboards; do
    curl -s "${api}/dashboards/uid/${dashboard}" | jq > "grafana/dashboard-${dashboard}.json"
done
