# coronarivus-grafana

The seeder populates an InfluxDB database which is then queried by Grafana to show nice graphs.

## License

AGPLv3+
